package com.pramit.fws

import android.app.Application
import com.pramit.fws.di.appModule
import com.pramit.fws.di.networkModule
import com.pramit.fws.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class GiphyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@GiphyApplication)
            modules(appModule, networkModule, viewModelModule)
        }
    }
}