package com.pramit.fws.di

import android.os.Build
import androidx.room.Room
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import coil.disk.DiskCache
import coil.memory.MemoryCache
import com.pramit.fws.BuildConfig
import com.pramit.fws.datasource.cache.database.GiphyDatabase
import com.pramit.fws.datasource.cache.repository.GifsCacheRepository
import com.pramit.fws.datasource.cache.repository.GifsCacheRepositoryImpl
import com.pramit.fws.datasource.network.api.ApiService
import com.pramit.fws.datasource.network.interceptor.GiphyApiInterceptor
import com.pramit.fws.datasource.network.mapper.GiphyNetworkMapper
import com.pramit.fws.datasource.network.repository.GifsRepository
import com.pramit.fws.datasource.network.repository.GifsRepositoryImpl
import com.pramit.fws.ui.favourite.FavouriteViewModel
import com.pramit.fws.ui.trending.TrendingViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {

    single {
        Room.databaseBuilder(
            androidContext(),
            GiphyDatabase::class.java,
            "Giphy.db"
        ).build()
    }

    single<GifsRepository> { GifsRepositoryImpl(get(), get(), GiphyNetworkMapper()) }

    single<GifsCacheRepository> { GifsCacheRepositoryImpl(get()) }

    single {
        ImageLoader.Builder(androidContext())
            .components {
                if (Build.VERSION.SDK_INT >= 28) {
                    add(ImageDecoderDecoder.Factory())
                } else {
                    add(GifDecoder.Factory())
                }
            }.build()
    }

}

val networkModule = module {
    single {
        OkHttpClient().newBuilder()
            .addInterceptor(GiphyApiInterceptor())
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level =
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BASIC else HttpLoggingInterceptor.Level.NONE
                })
            .build()
    }

    single {
        Retrofit.Builder().baseUrl(BuildConfig.GIPHY_BASE_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create()).build()
            .create(ApiService::class.java)
    }
}

val viewModelModule = module {
    viewModel { TrendingViewModel(get(), get(), get()) }
    viewModel { FavouriteViewModel(get(), get()) }
}