package com.pramit.fws.utils

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import coil.request.ImageRequest

fun View.hideKeyboard() {
    val inputMethodManager =
        context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
}

fun ImageView.imageRequest(url: String?): ImageRequest {
    return ImageRequest.Builder(this.context)
        .data(url)
        .crossfade(true)
        .target(this)
        .placeholder(ColorDrawable(GiphyPlaceholder.generate(this.context)))
        .build()
}