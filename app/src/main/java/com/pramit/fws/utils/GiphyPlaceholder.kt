package com.pramit.fws.utils

import android.content.Context
import androidx.core.content.ContextCompat
import com.pramit.fws.R

/**
 * Utility that generates a colored placeholder
 * to be used by imageview.
 */
object GiphyPlaceholder {

    private val placeholderColors = arrayOf(
        R.color.colorLightGreen,
        R.color.colorLightBlue,
        R.color.colorLightPurple,
        R.color.colorLightRed,
        R.color.colorLightYellow,
    )

    /**
     * Generates a random integer between 1..placeholderColors.size
     * (inclusive), which represents the resId for the placeholder color.
     */
    fun generate(context: Context): Int {
        val randomIndex = (1..placeholderColors.size).random()
        return ContextCompat.getColor(context, placeholderColors[randomIndex - 1])
    }
}