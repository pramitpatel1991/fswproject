package com.pramit.fws.common

object Constants {
    const val PAGE_LIMIT = 25
    const val INITIAL_OFFSET = 0
}