package com.pramit.fws.ui.favourite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.pramit.fws.databinding.FragmentFavouriteBinding
import com.pramit.fws.datasource.cache.entity.Favourites
import com.pramit.fws.ui.adapter.FavouriteAdapter
import com.pramit.fws.ui.adapter.FavouriteItemClickHandler
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavouriteFragment : Fragment(), FavouriteItemClickHandler {

    private lateinit var binding: FragmentFavouriteBinding
    private val viewModel: FavouriteViewModel by viewModel()
    private lateinit var adapter: FavouriteAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFavouriteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        setObservers()
    }

    private fun setUpRecyclerView() {
        adapter = FavouriteAdapter(viewModel.imageLoader, this)
        binding.rvFavouriteList.adapter = adapter
    }

    private fun setObservers() {
        viewModel.favouriteGifs.observe(viewLifecycleOwner) {
            binding.clEmptyView.isVisible = it.isEmpty()
            adapter.submitList(it)
        }
    }

    override fun onGiphyClick(favourites: Favourites) {
        viewModel.removeFromFavourite(favourites)
    }

}