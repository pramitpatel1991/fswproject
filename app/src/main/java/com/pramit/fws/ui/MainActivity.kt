package com.pramit.fws.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.pramit.fws.R
import com.pramit.fws.databinding.ActivityMainBinding
import com.pramit.fws.ui.adapter.GiphyPagerAdapter
import com.pramit.fws.ui.trending.TrendingFragment

class MainActivity : AppCompatActivity(), TabLayout.OnTabSelectedListener {

    lateinit var binding: ActivityMainBinding

    lateinit var giphyPagerAdapter: GiphyPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpViewPager()
        binding.swipeableTab.addOnTabSelectedListener(this)
    }

    private fun setUpViewPager() {
        giphyPagerAdapter = GiphyPagerAdapter(this)
        binding.viewPager.adapter = giphyPagerAdapter
        TabLayoutMediator(binding.swipeableTab, binding.viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = getString(R.string.tab_trending)
                1 -> tab.text = getString(R.string.tab_favourite)
            }
        }.attach()
    }

    override fun onTabSelected(tab: TabLayout.Tab?) {
        tab?.let {
            when (it.position) {
                0 -> {
                    val trending = giphyPagerAdapter.getFragmentByPosition(it.position)
                    if (trending is TrendingFragment) {
                        trending.updatePageIfRequired()
                    }
                }
            }
        }
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {
    }

    override fun onTabReselected(tab: TabLayout.Tab?) {
    }
}