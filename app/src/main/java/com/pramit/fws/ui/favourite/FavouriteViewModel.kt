package com.pramit.fws.ui.favourite

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import coil.ImageLoader
import com.pramit.fws.datasource.cache.entity.Favourites
import com.pramit.fws.datasource.cache.repository.GifsCacheRepository
import com.pramit.fws.ui.common.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavouriteViewModel(
    val imageLoader: ImageLoader,
    private val repo: GifsCacheRepository
) : BaseViewModel() {

    // Observer for favourites from database
    val favouriteGifs: LiveData<List<Favourites>> = repo.getAllFavourite()

    /**
     * Function to remove Gif from database
     * @param favourites - Favourite object
     */
    fun removeFromFavourite(favourites: Favourites) {
        viewModelScope.launch(Dispatchers.IO) {
            repo.deleteFavourite(favourites)
        }
    }

}