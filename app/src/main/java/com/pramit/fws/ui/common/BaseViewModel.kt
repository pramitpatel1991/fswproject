package com.pramit.fws.ui.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.pramit.fws.datasource.cache.entity.Giphy
import kotlinx.coroutines.flow.Flow

open class BaseViewModel : ViewModel()