package com.pramit.fws.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.pramit.fws.ui.favourite.FavouriteFragment
import com.pramit.fws.ui.trending.TrendingFragment

class GiphyPagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    private val fragmentList = arrayListOf(TrendingFragment(), FavouriteFragment())

    override fun getItemCount(): Int = fragmentList.size

    override fun createFragment(position: Int): Fragment {
        return fragmentList[position]
    }

    fun getFragmentByPosition(position: Int): Fragment {
        return fragmentList[position]
    }

}