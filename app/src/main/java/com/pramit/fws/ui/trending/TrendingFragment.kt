package com.pramit.fws.ui.trending

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import com.google.android.material.snackbar.Snackbar
import com.pramit.fws.R
import com.pramit.fws.databinding.FragmentTrendingBinding
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.ui.adapter.GifAdapter
import com.pramit.fws.ui.adapter.GiphyItemClickHandler
import com.pramit.fws.ui.adapter.GiphyLoadStateAdapter
import com.pramit.fws.utils.hideKeyboard
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class TrendingFragment : Fragment(), GiphyItemClickHandler {

    private lateinit var binding: FragmentTrendingBinding
    private lateinit var adapter: GifAdapter
    private lateinit var searchAdapter: GifAdapter
    private val viewModel: TrendingViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrendingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        adapter = GifAdapter(viewModel.imageLoader, this)
        initAdapter(adapter, true)
        setObservers()
    }

    private fun setObservers() {
        viewModel.dataSource.observe(viewLifecycleOwner) {
            lifecycleScope.launch {
                adapter.submitData(it)
            }
        }
    }

    fun updatePageIfRequired() {
        if (viewModel.isSearchState()) {
            initAdapter(adapter, true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.actionSearch)
        val searchView = searchItem.actionView as SearchView
        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                searchView.hideKeyboard()
                viewModel.setSearchState(false)
                initAdapter(adapter, true)
                return true
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.hideKeyboard()
                if (!query.isNullOrEmpty()) {
                    viewModel.setSearchState(true)
                    viewModel.searchGifs(query)
                    searchAdapter()
                    searchAdapter.refresh()
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    private fun initAdapter(adapter: GifAdapter, isMediator: Boolean) {
        binding.rvTrendingList.adapter = adapter

        binding.rvTrendingList.adapter = adapter.withLoadStateFooter(
            footer = GiphyLoadStateAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            val refreshState =
                if (isMediator) loadState.mediator?.refresh else loadState.source.refresh
            binding.rvTrendingList.isVisible = refreshState is LoadState.NotLoading
            binding.progressBar.isVisible = refreshState is LoadState.Loading
            binding.btnRetry.isVisible = refreshState is LoadState.Error
            binding.tvError.isVisible = refreshState is LoadState.Error
            handleError(loadState)
        }

        binding.refresh.setOnRefreshListener {
            adapter.refresh()
            binding.refresh.isRefreshing = false
        }
        binding.btnRetry.setOnClickListener {
            adapter.retry()
        }
    }

    private fun searchAdapter() {
        searchAdapter = GifAdapter(viewModel.imageLoader, this)
        binding.rvTrendingList.adapter = searchAdapter
        viewModel.searchGiphyFlow.observe(viewLifecycleOwner) {
            lifecycleScope.launch {
                searchAdapter.submitData(it)
            }
        }
        initAdapter(searchAdapter, false)
    }

    private fun handleError(loadState: CombinedLoadStates) {
        val errorState = loadState.source.append as? LoadState.Error
            ?: loadState.source.prepend as? LoadState.Error
        errorState?.let {
            Snackbar.make(
                requireContext(),
                binding.root,
                getString(R.string.error_message_something_went_wrong),
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    override fun onGiphyClick(giphy: Giphy) {
        viewModel.markFavourite(giphy)
    }

}