package com.pramit.fws.ui.trending

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import coil.ImageLoader
import com.pramit.fws.datasource.cache.entity.Favourites
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.cache.repository.GifsCacheRepository
import com.pramit.fws.datasource.network.repository.GifsRepository
import com.pramit.fws.ui.common.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TrendingViewModel(
    val imageLoader: ImageLoader,
    private val repository: GifsRepository,
    private val cacheRepository: GifsCacheRepository
) : BaseViewModel() {

    private var isSearch = false

    private var _searchGiphyList: LiveData<PagingData<Giphy>> = MutableLiveData()
    val searchGiphyFlow: LiveData<PagingData<Giphy>>
        get() = _searchGiphyList

    private var _dataSource: LiveData<PagingData<Giphy>> = MutableLiveData()
    val dataSource: LiveData<PagingData<Giphy>>
        get() = _dataSource

    init {
        _dataSource = repository.getGiphyFromMediator().cachedIn(viewModelScope)
    }

    fun isSearchState(): Boolean = isSearch

    fun setSearchState(search: Boolean) {
        isSearch = search
    }

    fun searchGifs(query: String) {
        _searchGiphyList = repository.searchGiphy(query).cachedIn(viewModelScope)
    }

    fun markFavourite(giphy: Giphy) {
        viewModelScope.launch(Dispatchers.IO) {
            if (cacheRepository.isFavourite(giphy.id) > 0) {
                cacheRepository.deleteFavouriteByGiphy(giphy.id)
            } else {
                cacheRepository.insertFavourite(Favourites(giphyId = giphy.id, url = giphy.url))
            }
        }
    }

}