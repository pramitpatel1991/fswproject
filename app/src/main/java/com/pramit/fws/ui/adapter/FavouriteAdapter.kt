package com.pramit.fws.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.ImageLoader
import com.pramit.fws.databinding.RowItemGifBinding
import com.pramit.fws.datasource.cache.entity.Favourites
import com.pramit.fws.utils.imageRequest

interface FavouriteItemClickHandler {
    fun onGiphyClick(favourites: Favourites)
}

class FavouriteAdapter(
    private val imageLoader: ImageLoader,
    private val onClickListener: FavouriteItemClickHandler
) : ListAdapter<Favourites, FavouriteViewHolder>(RowDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder {
        val binding = RowItemGifBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FavouriteViewHolder(binding, imageLoader, onClickListener)
    }

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    private class RowDiffCallback : DiffUtil.ItemCallback<Favourites>() {
        override fun areItemsTheSame(oldItem: Favourites, newItem: Favourites): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Favourites, newItem: Favourites): Boolean {
            return oldItem.id == newItem.id && oldItem.url == newItem.url
        }
    }

}

class FavouriteViewHolder(
    private val rowItemGifBinding: RowItemGifBinding,
    private val imageLoader: ImageLoader,
    private val onClickListener: FavouriteItemClickHandler
) : RecyclerView.ViewHolder(rowItemGifBinding.root) {

    fun bind(favourites: Favourites) {
        imageLoader.enqueue(rowItemGifBinding.image.imageRequest(favourites.url))
        rowItemGifBinding.fav.isChecked = true
        rowItemGifBinding.fav.setOnClickListener {
            onClickListener.onGiphyClick(favourites)
        }
    }
}