package com.pramit.fws.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.ImageLoader
import com.pramit.fws.databinding.RowItemGifBinding
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.utils.imageRequest

interface GiphyItemClickHandler {
    fun onGiphyClick(giphy: Giphy)
}

class GifAdapter(
    private val imageLoader: ImageLoader,
    private val onClickListener: GiphyItemClickHandler
) : PagingDataAdapter<Giphy, GifViewHolder>(GiphyRowDiffCallback()) {

    lateinit var binding: RowItemGifBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GifViewHolder {
        binding = RowItemGifBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GifViewHolder(binding, imageLoader, onClickListener)
    }

    override fun onBindViewHolder(holder: GifViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    private class GiphyRowDiffCallback : DiffUtil.ItemCallback<Giphy>() {
        override fun areItemsTheSame(oldItem: Giphy, newItem: Giphy): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Giphy, newItem: Giphy): Boolean {
            return oldItem.id == newItem.id && oldItem.url == newItem.url && oldItem.isFav == newItem.isFav
        }
    }

}

class GifViewHolder(
    private val rowItemGifBinding: RowItemGifBinding,
    private val imageLoader: ImageLoader,
    private val onClickListener: GiphyItemClickHandler
) : RecyclerView.ViewHolder(rowItemGifBinding.root) {

    fun bind(giphy: Giphy) {
        rowItemGifBinding.fav.isChecked = giphy.isFav
        imageLoader.enqueue(rowItemGifBinding.image.imageRequest(giphy.url))
        rowItemGifBinding.fav.setOnClickListener {
            onClickListener.onGiphyClick(giphy)
        }
    }
}