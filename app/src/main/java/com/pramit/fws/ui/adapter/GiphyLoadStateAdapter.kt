package com.pramit.fws.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.pramit.fws.R
import com.pramit.fws.databinding.ItemRetryLoadStateBinding

class GiphyLoadStateAdapter(
    private val retry: () -> Unit
) : LoadStateAdapter<GiphyLoadStateViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): GiphyLoadStateViewHolder {
        val binding =
            ItemRetryLoadStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GiphyLoadStateViewHolder(binding, retry)
    }

    override fun onBindViewHolder(holder: GiphyLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }
}

class GiphyLoadStateViewHolder(
    private val binding: ItemRetryLoadStateBinding,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.retryButton.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            binding.tvError.text = binding.tvError.context.getString(R.string.error_network_message)
        }
        binding.progressBar.isVisible = loadState is LoadState.Loading
        binding.retryButton.isVisible = loadState !is LoadState.Loading
        binding.tvError.isVisible = loadState !is LoadState.Loading
    }
}
