package com.pramit.fws.datasource.cache.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pramit.fws.datasource.cache.entity.Giphy

@Dao
interface GiphyDao {

    @Query("Select gif.id, gif.url, (Select 1 from Favourites where giphyId = gif.id) as isFav from giphy as gif")
    fun getAllTrendingGifs(): PagingSource<Int, Giphy>

    @Query("DELETE FROM giphy")
    suspend fun deleteAllTrendingGifs()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllTrendingGifs(list: List<Giphy>)

}