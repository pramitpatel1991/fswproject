package com.pramit.fws.datasource.cache.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_keys")
data class RemoteKey(
    @PrimaryKey val giphyId: String,
    val prevKey: Int?,
    val nextKey: Int?
)
