package com.pramit.fws.datasource.cache.repository

import androidx.lifecycle.LiveData
import com.pramit.fws.datasource.cache.entity.Favourites

interface GifsCacheRepository {
    suspend fun isFavourite(id: String): Int

    suspend fun insertFavourite(favourites: Favourites)

    suspend fun deleteFavourite(favourites: Favourites)

    suspend fun deleteFavouriteByGiphy(id: String)

    fun getAllFavourite(): LiveData<List<Favourites>>
}