package com.pramit.fws.datasource.cache.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "giphy")
data class Giphy(
    @PrimaryKey
    val id: String,
    val url: String,
    var isFav: Boolean = false
)
