package com.pramit.fws.datasource.network.api

import com.pramit.fws.datasource.network.model.GiphyResponseModel
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("trending")
    suspend fun fetchTrending(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): GiphyResponseModel

    @GET("search")
    suspend fun searchGifs(
        @Query("limit") limit: Int, @Query("offset") offset: Int,
        @Query("q") query: String
    ): GiphyResponseModel

}
