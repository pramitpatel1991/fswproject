package com.pramit.fws.datasource.network.repository

import androidx.lifecycle.LiveData
import androidx.paging.*
import com.pramit.fws.common.Constants.PAGE_LIMIT
import com.pramit.fws.datasource.cache.database.GiphyDatabase
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.network.api.ApiService
import com.pramit.fws.datasource.network.mapper.GiphyNetworkMapper
import com.pramit.fws.datasource.network.paging.GiphyRemoteMediator
import com.pramit.fws.datasource.network.paging.SearchPagingDataSource

class GifsRepositoryImpl(
    private val api: ApiService,
    private val database: GiphyDatabase,
    private val mapper: GiphyNetworkMapper
): GifsRepository {

    @OptIn(ExperimentalPagingApi::class)
    override fun getGiphyFromMediator(): LiveData<PagingData<Giphy>> {
        val pagingSourceFactory = { database.giphyDao().getAllTrendingGifs() }

        return Pager(
            config = PagingConfig(
                pageSize = PAGE_LIMIT,
                maxSize = PAGE_LIMIT * 3,
                enablePlaceholders = false
            ),
            remoteMediator = GiphyRemoteMediator(api, database, mapper),
            pagingSourceFactory = pagingSourceFactory
        ).liveData
    }

    override fun searchGiphy(query: String): LiveData<PagingData<Giphy>> {
        return Pager(
            config = PagingConfig(
                pageSize = PAGE_LIMIT,
                maxSize = PAGE_LIMIT * 3,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { SearchPagingDataSource(api, query, mapper, database) }
        ).liveData
    }

}