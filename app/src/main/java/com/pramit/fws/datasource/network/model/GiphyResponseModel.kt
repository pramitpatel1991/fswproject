package com.pramit.fws.datasource.network.model

import com.google.gson.annotations.SerializedName

data class GiphyResponseModel(
    @SerializedName("data")
    var data: List<GiphyNetworkModel>,
    @SerializedName("meta")
    var meta: Meta,
    @SerializedName("pagination")
    var pagination: Pagination
)

data class GiphyNetworkModel(
    @SerializedName("bitly_gif_url")
    var bitlyGifUrl: String?,
    @SerializedName("bitly_url")
    var bitlyUrl: String?,
    @SerializedName("content_url")
    var contentUrl: String?,
    @SerializedName("embed_url")
    var embedUrl: String?,
    @SerializedName("id")
    var id: String = "",
    @SerializedName("images")
    var images: Images?,
    @SerializedName("import_datetime")
    var importDatetime: String?,
    @SerializedName("is_sticker")
    var isSticker: Int?,
    @SerializedName("rating")
    var rating: String?,
    @SerializedName("slug")
    var slug: String?,
    @SerializedName("source")
    var source: String?,
    @SerializedName("source_post_url")
    var sourcePostUrl: String?,
    @SerializedName("source_tld")
    var sourceTld: String?,
    @SerializedName("title")
    var title: String?,
    @SerializedName("trending_datetime")
    var trendingDatetime: String?,
    @SerializedName("type")
    var type: String?,
    @SerializedName("url")
    var gpUrl: String?,
    @SerializedName("username")
    var username: String?
)

data class Images(
    @SerializedName("fixed_width")
    var fixedWidth: FixedWidth,
)

data class FixedWidth(
    @SerializedName("height")
    var height: String,
    @SerializedName("mp4")
    var mp4: String,
    @SerializedName("mp4_size")
    var mp4Size: String,
    @SerializedName("size")
    var size: String,
    @SerializedName("url")
    var url: String,
    @SerializedName("webp")
    var webp: String,
    @SerializedName("webp_size")
    var webpSize: String,
    @SerializedName("width")
    var width: String
)

data class Meta(
    @SerializedName("msg")
    var msg: String,
    @SerializedName("response_id")
    var responseId: String,
    @SerializedName("status")
    var status: Int
)

data class Pagination(
    @SerializedName("count")
    var count: Int,
    @SerializedName("offset")
    var offset: Int,
    @SerializedName("total_count")
    var totalCount: Int
)