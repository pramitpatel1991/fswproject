package com.pramit.fws.datasource.cache.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favourites")
data class Favourites(
    @PrimaryKey(autoGenerate = true)
    var id:Int = 0,
    var giphyId: String,
    var url: String?
)