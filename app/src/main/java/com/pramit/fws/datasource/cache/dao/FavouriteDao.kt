package com.pramit.fws.datasource.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.pramit.fws.datasource.cache.entity.Favourites

@Dao
interface FavouriteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addToFavourite(fav: Favourites)

    @Query("DELETE FROM favourites WHERE id =:id")
    suspend fun removeFromFavourite(id: Int)

    @Query("DELETE FROM favourites WHERE giphyId =:id")
    suspend fun removeFromFavouriteByGiphyId(id: String)

    @Query("SELECT * FROM favourites ORDER BY id DESC")
    fun getAllFavourite(): LiveData<List<Favourites>>

    @Query("SELECT * FROM favourites WHERE giphyId =:id")
    fun getFavouriteById(id: String): Favourites

    @Query("SELECT COUNT(id) FROM favourites WHERE giphyId = :id")
    suspend fun isFavourite(id: String): Int
}