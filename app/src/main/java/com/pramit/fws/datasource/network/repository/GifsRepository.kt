package com.pramit.fws.datasource.network.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagingData
import com.pramit.fws.datasource.cache.entity.Giphy

interface GifsRepository {

    fun getGiphyFromMediator(): LiveData<PagingData<Giphy>>

    fun searchGiphy(query: String): LiveData<PagingData<Giphy>>
}