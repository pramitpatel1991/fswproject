package com.pramit.fws.datasource.network.paging

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.pramit.fws.common.Constants.INITIAL_OFFSET
import com.pramit.fws.common.Constants.PAGE_LIMIT
import com.pramit.fws.datasource.cache.database.GiphyDatabase
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.cache.entity.RemoteKey
import com.pramit.fws.datasource.network.api.ApiService
import com.pramit.fws.datasource.network.mapper.GiphyNetworkMapper
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class GiphyRemoteMediator(
    private val api: ApiService,
    private val db: GiphyDatabase,
    private val mapper: GiphyNetworkMapper
) : RemoteMediator<Int, Giphy>() {


    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, Giphy>
    ): MediatorResult {

        var page = when (val keyData = getKey(loadType, state)) {
            is MediatorResult.Success -> return keyData
            else -> keyData as Int
        }

        try {
            val response = api.fetchTrending(offset = page, limit = state.config.pageSize)
            val giphyList = mapper.mapFromEntity(response)
            val endOfPaginationReached = response.data.isEmpty()
            db.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    page = INITIAL_OFFSET
                    db.giphyDao().deleteAllTrendingGifs()
                    db.remoteKeyDao().deleteAll()
                }
                val prevKey = if (page == INITIAL_OFFSET) null else page
                val nextKey = page + PAGE_LIMIT
                val keys = response.data.map {
                    RemoteKey(it.id, prevKey = prevKey, nextKey = nextKey)
                }
                db.remoteKeyDao().insertAll(keys)
                giphyList.forEach { giphy ->
                    if (db.favouriteDao().isFavourite(giphy.id) > 0) giphy.isFav = true
                }
                db.giphyDao().insertAllTrendingGifs(giphyList)
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getKey(loadType: LoadType, state: PagingState<Int, Giphy>): Any {
        return when (loadType) {
            //when it's the first time we're loading data
            LoadType.REFRESH -> {
                INITIAL_OFFSET
            }
            //When we need to load data at the beginning of the currently loaded data set:
            LoadType.PREPEND -> {
                val remoteKeys = getFirstRemoteKey(state)
                val prevKey =
                    remoteKeys?.prevKey ?: MediatorResult.Success(endOfPaginationReached = false)
                prevKey
            }
            //When we need to load data at the end of the currently loaded data set:
            LoadType.APPEND -> {
                val remoteKeys = getLastRemoteKey(state)
                remoteKeys?.nextKey ?: MediatorResult.Success(endOfPaginationReached = false)
            }
        }
    }

    private suspend fun getLastRemoteKey(state: PagingState<Int, Giphy>): RemoteKey? {
        return state.pages
            .lastOrNull { it.data.isNotEmpty() }
            ?.data?.lastOrNull()
            ?.let { giphy -> db.remoteKeyDao().remoteKeysGiphyId(giphy.id) }
    }

    private suspend fun getFirstRemoteKey(state: PagingState<Int, Giphy>): RemoteKey? {
        return state.pages
            .firstOrNull { it.data.isNotEmpty() }
            ?.data?.firstOrNull()
            ?.let { giphy -> db.remoteKeyDao().remoteKeysGiphyId(giphy.id) }
    }


}