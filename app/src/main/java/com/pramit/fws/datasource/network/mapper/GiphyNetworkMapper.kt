package com.pramit.fws.datasource.network.mapper

import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.network.model.GiphyResponseModel
import com.pramit.fws.datasource.util.EntityMapper

class GiphyNetworkMapper : EntityMapper<GiphyResponseModel, List<Giphy>> {

    override fun mapFromEntity(entity: GiphyResponseModel): List<Giphy> {
        return entity.data.map { giphyItem ->
            Giphy(
                id = giphyItem.id,
                url = giphyItem.images!!.fixedWidth.url
            )
        }
    }
}