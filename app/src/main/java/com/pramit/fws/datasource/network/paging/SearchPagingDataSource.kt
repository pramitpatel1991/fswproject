package com.pramit.fws.datasource.network.paging

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.pramit.fws.common.Constants.INITIAL_OFFSET
import com.pramit.fws.common.Constants.PAGE_LIMIT
import com.pramit.fws.datasource.cache.database.GiphyDatabase
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.network.api.ApiService
import com.pramit.fws.datasource.network.mapper.GiphyNetworkMapper

class SearchPagingDataSource(
    private val apiService: ApiService,
    private val query: String,
    private val mapper: GiphyNetworkMapper,
    private val database: GiphyDatabase
) : PagingSource<Int, Giphy>() {

    override fun getRefreshKey(state: PagingState<Int, Giphy>): Int {
        return INITIAL_OFFSET
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Giphy> {
        val pageNumber = params.key ?: INITIAL_OFFSET
        return try {
            val response = apiService.searchGifs(PAGE_LIMIT, pageNumber, query)

            val giphyList = mapper.mapFromEntity(response)
            giphyList.forEach { giphy ->
                if (database.favouriteDao().isFavourite(giphy.id) > 0) giphy.isFav = true
            }
            val nextPageNumber: Int? = if (giphyList.isEmpty()) {
                null
            } else {
                response.pagination.offset + PAGE_LIMIT
            }
            LoadResult.Page(
                data = giphyList,
                prevKey = null,
                nextKey = nextPageNumber
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}