package com.pramit.fws.datasource.cache.repository

import androidx.lifecycle.LiveData
import com.pramit.fws.datasource.cache.database.GiphyDatabase
import com.pramit.fws.datasource.cache.entity.Favourites

class GifsCacheRepositoryImpl(private val database: GiphyDatabase) : GifsCacheRepository {

    override suspend fun isFavourite(id: String): Int {
        return database.favouriteDao().isFavourite(id)
    }

    override suspend fun insertFavourite(favourites: Favourites) {
        database.favouriteDao().addToFavourite(favourites)
    }

    override suspend fun deleteFavourite(favourites: Favourites) {
        database.favouriteDao().removeFromFavourite(favourites.id)
    }

    override suspend fun deleteFavouriteByGiphy(id: String) {
        database.favouriteDao().removeFromFavouriteByGiphyId(id)
    }

    override fun getAllFavourite(): LiveData<List<Favourites>> {
        return database.favouriteDao().getAllFavourite()
    }

}