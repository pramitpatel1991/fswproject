package com.pramit.fws.datasource.cache.database

import androidx.room.Database
import com.pramit.fws.datasource.cache.dao.FavouriteDao
import com.pramit.fws.datasource.cache.dao.GiphyDao
import com.pramit.fws.datasource.cache.dao.RemoteKeyDao
import com.pramit.fws.datasource.cache.entity.Favourites
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.cache.entity.RemoteKey

@Database(entities = [Giphy::class, RemoteKey::class, Favourites::class], version = 1)
abstract class GiphyDatabase : androidx.room.RoomDatabase() {

    abstract fun giphyDao(): GiphyDao

    abstract fun remoteKeyDao(): RemoteKeyDao

    abstract fun favouriteDao(): FavouriteDao
}