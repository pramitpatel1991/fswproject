package com.pramit.fws.ui.favourite

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import coil.ImageLoader
import com.pramit.fws.datasource.cache.entity.Favourites
import com.pramit.fws.datasource.cache.repository.GifsCacheRepositoryImpl
import io.mockk.MockKAnnotations
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class FavouriteViewModelTest {

    @MockK
    lateinit var gifsCacheRepository: GifsCacheRepositoryImpl

    @MockK
    lateinit var imageLoader: ImageLoader

    lateinit var underTest: FavouriteViewModel

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxed = true, relaxUnitFun = true)
        underTest = FavouriteViewModel(imageLoader, gifsCacheRepository)
    }

    @Test
    fun `should remove favourite from local cache and show removed message`() = runTest {
        val favourites = mockk<Favourites>()
        underTest.removeFromFavourite(favourites)
        coVerify { gifsCacheRepository.deleteFavourite(favourites) }
        Assert.assertNotNull(underTest.message.getOrAwaitValue())

    }

}