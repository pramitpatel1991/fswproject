package com.pramit.fws.ui.datasource.network.paging

import androidx.paging.PagingSource
import com.pramit.fws.common.Constants
import com.pramit.fws.datasource.cache.dao.FavouriteDao
import com.pramit.fws.datasource.cache.database.GiphyDatabase
import com.pramit.fws.datasource.network.api.ApiService
import com.pramit.fws.datasource.network.mapper.GiphyNetworkMapper
import com.pramit.fws.datasource.network.model.*
import com.pramit.fws.datasource.network.paging.SearchPagingDataSource
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class SearchPagingDataSourceTest {

    @MockK
    lateinit var apiService: ApiService

    @MockK
    lateinit var mapper: GiphyNetworkMapper

    @MockK
    lateinit var database: GiphyDatabase

    @MockK
    lateinit var favouriteDao: FavouriteDao

    @MockK
    lateinit var giphyNetworkModel: GiphyNetworkModel

    lateinit var underTest: SearchPagingDataSource

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxed = true, relaxUnitFun = true)
        val query = "test"
        every { giphyNetworkModel.id } returns "123"
        val fixedWidth1 = mockk<FixedWidth>()
        every { fixedWidth1.url } returns "www.google.com"
        every { giphyNetworkModel.images } returns Images(fixedWidth1)
        underTest = SearchPagingDataSource(apiService, query, mapper, database)
    }

    @Test
    fun `should fetch data from network`() = runTest {
        val giphyResponse = GiphyResponseModel(
            data = arrayListOf(giphyNetworkModel),
            mockk(),
            Pagination(offset = 0, count = 25, totalCount = 25)
        )
        coEvery { apiService.searchGifs(any(), any(), any()) } returns giphyResponse
        val giphyList = GiphyNetworkMapper().mapFromEntity(giphyResponse)
        every { mapper.mapFromEntity(any()) } returns giphyList
        Assert.assertEquals(
            PagingSource.LoadResult.Page(
                data = giphyList,
                prevKey = null,
                nextKey = 25
            ),
            underTest.load(
                PagingSource.LoadParams.Refresh(
                    key = 0,
                    loadSize = Constants.PAGE_LIMIT * 3,
                    placeholdersEnabled = false
                )
            )
        )
    }

    @Test
    fun `should fetch data from network and set true when item already added to database`() =
        runTest {
            coEvery { favouriteDao.isFavourite(any()) } returns 1
            every { database.favouriteDao() } returns favouriteDao
            val giphyResponse = GiphyResponseModel(
                data = arrayListOf(giphyNetworkModel),
                mockk(),
                Pagination(offset = 0, count = 25, totalCount = 25)
            )
            coEvery { apiService.searchGifs(any(), any(), any()) } returns giphyResponse
            val giphyList = GiphyNetworkMapper().mapFromEntity(giphyResponse)
            every { mapper.mapFromEntity(any()) } returns giphyList
            val actual = underTest.load(
                PagingSource.LoadParams.Refresh(
                    key = 0,
                    loadSize = Constants.PAGE_LIMIT * 3,
                    placeholdersEnabled = false
                )
            )
            val expected = PagingSource.LoadResult.Page(giphyList, null, 25)
            Assert.assertEquals(expected, actual)
            Assert.assertEquals(true, giphyList[0].isFav)
        }

}