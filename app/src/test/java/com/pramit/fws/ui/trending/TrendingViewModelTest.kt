package com.pramit.fws.ui.trending

import coil.ImageLoader
import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.cache.repository.GifsCacheRepositoryImpl
import com.pramit.fws.datasource.network.repository.GifsRepositoryImpl
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(JUnit4::class)
class TrendingViewModelTest {

    @MockK
    lateinit var gifsCacheRepository: GifsCacheRepositoryImpl

    @MockK
    lateinit var gifsRepository: GifsRepositoryImpl

    @MockK
    lateinit var imageLoader: ImageLoader

    lateinit var underTest: TrendingViewModel

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxed = true, relaxUnitFun = true)
        underTest = TrendingViewModel(imageLoader, gifsRepository, gifsCacheRepository)
    }

    @Test
    fun `should add gif to favourite when its not added`() = runTest {
        val giphy = Giphy("123", "www.test.com")
        coEvery { gifsCacheRepository.isFavourite(any()) } returns 0
        underTest.markFavourite(giphy)
        coVerify{ gifsCacheRepository.insertFavourite(any()) }
    }

    @Test
    fun `should remove gif from favourite when its already added`() = runTest {
        val giphy = Giphy("123", "www.test.com")
        coEvery { gifsCacheRepository.isFavourite(any()) } returns 1
        underTest.markFavourite(giphy)
        coVerify{ gifsCacheRepository.deleteFavouriteByGiphy(any()) }
    }


}