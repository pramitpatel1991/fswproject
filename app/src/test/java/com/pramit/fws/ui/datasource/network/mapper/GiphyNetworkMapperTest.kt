package com.pramit.fws.ui.datasource.network.mapper

import com.pramit.fws.datasource.cache.entity.Giphy
import com.pramit.fws.datasource.network.mapper.GiphyNetworkMapper
import com.pramit.fws.datasource.network.model.FixedWidth
import com.pramit.fws.datasource.network.model.GiphyNetworkModel
import com.pramit.fws.datasource.network.model.GiphyResponseModel
import com.pramit.fws.datasource.network.model.Images
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class GiphyNetworkMapperTest {

    lateinit var underTest: GiphyNetworkMapper

    @Test
    fun `should convert network entity to domain`() {
        underTest = GiphyNetworkMapper()
        val giphyNetworkModel1 = mockk<GiphyNetworkModel>()
        every { giphyNetworkModel1.id } returns  "123"
        val fixedWidth1 = mockk<FixedWidth>()
        every { fixedWidth1.url } returns "www.google.com"
        every { giphyNetworkModel1.images } returns Images(fixedWidth1)
        val giphyNetworkModel2 = mockk<GiphyNetworkModel>()
        every { giphyNetworkModel2.id } returns  "456"
        val fixedWidth2 = mockk<FixedWidth>()
        every { fixedWidth2.url } returns "www.amazon.com"
        every { giphyNetworkModel2.images } returns Images(fixedWidth2)
        val giphyResponseModel = GiphyResponseModel(
            data = arrayListOf(giphyNetworkModel1, giphyNetworkModel2), mockk(), mockk()
        )
        val giphys = underTest.mapFromEntity(giphyResponseModel)
        Assert.assertNotNull(giphys)
        Assert.assertEquals(2, giphys.size)
        Assert.assertEquals(Giphy("123","www.google.com"),giphys[0])
        Assert.assertEquals(Giphy("456","www.amazon.com"),giphys[1])
    }
}