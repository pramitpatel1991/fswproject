# FSWProject
Application is build using **Kotlin Programming Language and MVVM architecture**. 
Below are the Third party libraries used -
- **Retrofit 2** - For Network call
- **Coil** - To load Gif images
- **Koin** - Kotlin Dependency Injection
- **ROOM** - Persisting Storage to store favourite gif data locally.
- **Coroutine** - Structured Concurrency
- **Android JetPack Component** - ViewModel, LiveData, Room, Paging 3
